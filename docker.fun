:<<\_c
-e REGISTRY_HTTP_ADDR=${MNIC_NETWORK}:443  # specifies the port the registry listens to within the container

https://gabrieltanner.org/blog/docker-registry
https://docs.docker.com/registry/deploying/
_c
:<<\_x
. docker.fun && _docker_install
_docker_pull ubuntu
docker images
_x

#Sep 26 15:18:35 dockerhub docker[5357]: 2023/09/26 19:18:35 http: TLS handshake error from 192.168.1.15:32878: remote error: tls: bad certificate

sudo mkdir -p /etc/docker

REGISTRY_IMAGE=${REGISTRY_IMAGE:-registry:2}
#REGISTRY_IMAGE=${REGISTRY_IMAGE:-registry:2.1.1}

:<<\_c
to be explicit paths in the container's independent filesystem are declared as INTERNAL_*
_c
INTERNAL_REGISTRY_PORT=5000
INTERNAL_CERTS_PATH=/certs
INTERNAL_CERT=$INTERNAL_CERTS_PATH/domain.cert
INTERNAL_KEY=$INTERNAL_CERTS_PATH/domain.key
INTERNAL_CONFIG_YML=/etc/docker/registry/config.yml
INTERNAL_WORK_DIR=/var/lib/registry

EXTERNAL_WORK_DIR=${DOCKERHUB_DATA_PATH:-/var/lib/docker}

#echo hglwoowfs
#DEBUG=2

# ----------
:<<\_c
https://stackoverflow.com/questions/45023363/what-is-docker-io-in-relation-to-docker-ce-and-docker-ee-now-called-mirantis-k#:~:text=The%20accepted%20answer%20is%20under,repository%20from%20docker.com%20beforehands.
https://askubuntu.com/questions/906289/docker-ce-or-docker-io-package
_c

:<<\_c
method=gpg
install current version of docker (iow, not the distro version which is likely dated)
this is the docker.com documented method; however, it can conflict with disto installs. using this method, ceph-ansible exibits a problem where docker.service fails to load.
https://docs.docker.com/engine/install/ubuntu/
_c

:<<\_c
note1:

Kolla-ansible adds the Docker repo to the system's sources list as follows...
echo "deb https://download.docker.com/linux/ubuntu focal stable" > /etc/apt/sources.list.d/docker.list

Docker documentation says to add the Docker repo to the system's sources list as follows...
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" > /etc/apt/sources.list.d/docker.list

the difference betwen these source list entries results in this error...
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}

to circumvent this issue, rather than create a source list entry like Docker documentation recommends, we create a source list entry like the Kolla-ansible one, since we have to conform to kolla-ansible's automation methods

using this...
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

retults in this:
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}

using this...
echo "deb https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

results in this:
    default: W: GPG error: https://download.docker.com/linux/ubuntu focal InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 7EA0A9C3F273FCD8
    default: E: The repository 'https://download.docker.com/linux/ubuntu focal InRelease' is not signed.

therefore, the only way i see we can follow Docker's documented install is to use their recommended settings and then remove the sources list after installation is complete
_c

function _docker_install() {

#echo "_docker_install sfdsf"

which docker 1>/dev/null && return 0

_install jq

:<<\_c
method=pkg
this is the distro method that ubuntu ceph-absible is compatible with
_c
#method=apt-key  # deprecated
#method=pkg  # distro-method
#method=gpg
#method=script
method=test

case $method in

kolla)
curl -sSL https://get.docker.io | sudo bash
;;

test)

_debug test1

_install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

_debug test2

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
:<<\_x
ll /usr/share/keyrings/
_x

_debug test3

:<<\_c
Make sure /usr/share/keyrings/docker-archive-keyring.gpg is global readable.
the gpg file must be 'o+r', we could run 'chmod o+r'; however that would not catch the situation where the permission change does not take due to disk mount conditions or the like
here, we explicitly check for the right conditions and report if there was a problem
_c
sudo -u nobody test -r /usr/share/keyrings/docker-archive-keyring.gpg || _return 'sudo -u nobody test -r /usr/share/keyrings/docker-archive-keyring.gpg'

_debug test4

#echo \
#  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
#  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

:<<\_s
FAIL...
echo \
  "deb https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null
_s

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

_debug test5 "$(which docker)"

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

_debug test6

_install docker-ce docker-ce-cli containerd.io || _return '_install docker-ce docker-ce-cli containerd.io'

_debug test7

sudo rm -f /etc/apt/sources.list.d/docker.list  # see note1 above
;;

pkg)
_install docker.io

#sudo usermod -aG docker $USER
;;

gpg)

_install apt-transport-https ca-certificates curl gnupg lsb-release

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key --keyring /etc/apt/trusted.gpg.d/docker-apt-key.gpg add
:<<\_x
sudo gpg --keyserver subkeys.pgp.net --recv-keys 0EBFCD88

sudo apt-key fingerprint 0EBFCD88 1>/dev/null || _exit 'apt-key fingerprint 0EBFCD88'
_x


#echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/trusted.gpg.d/docker-apt-key.gpg] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

:<<\_x
cat /etc/apt/sources.list.d/docker.list
_x

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

:<<\_c
docker-ce is provided by docker.com, docker.io is provided by Debian
https://stackoverflow.com/questions/45023363/what-is-docker-io-in-relation-to-docker-ce-and-docker-ee-now-called-mirantis-k#:~:text=The%20accepted%20answer%20is%20under,repository%20from%20docker.com%20beforehands.
_c
_install docker-ce docker-ce-cli containerd.io || _return '_install docker-ce docker-ce-cli containerd.io'

sudo rm /etc/apt/sources.list.d/docker.list  # see note1 above

#sudo usermod -aG docker $USER
;;

apt-key)

_install software-properties-common

#wget -qO - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo apt-add-repository -y 'deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable'

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
#deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable

#sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

:<<\_s
sudo apt --yes --no-install-recommends install apt-transport-https ca-certificates
wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release --codename --short) stable"
_s

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-ce-archive-keyring.gpg 1>/dev/null

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

_install docker.io || _return '_install docker.io'

;;

script)

:<<\_c
this results in...
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}
_c

wget -qO- https://get.docker.com/ | sudo sh || _return 'wget -qO- https://get.docker.com/ | sudo sh'

;;

esac

sudo usermod -aG docker $USER


# ----------
sudo systemctl --now enable containerd.service || _return 'sudo systemctl --now enable containerd.service'

sudo systemctl --now enable docker.service || _return 'sudo systemctl --now enable docker.service'

:<<\_x
sudo systemctl status containerd.service
sudo systemctl status docker.service
_x

:<<\_s
# this is default...

sudo mkdir -p /var/lib/docker

sudo bash -c "cat > /etc/docker/daemon.json" <<!
{ "data-root": "/var/lib/docker", "storage-driver": "overlay2" }
!

sudo systemctl daemon-reload
sudo systemctl restart docker.service
_s


:<<\_x
sudo dockerd --debug
_x

:<<\_c
docker works out of the box for 'root', but fails for other users due to the fact that they are not actvie members of the group 'docker'

does this overcome the need for user to be in the 'docker' group?

https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue
_c
#[[ "$PROVISIONER_USER" == 'root' ]] || sudo chmod 660 /var/run/docker.sock


:<<\_s
# ----------
# allow current user to use docker

_debug "USER: $USER"

sudo groupadd --force docker
sudo usermod -aG docker $USER
:<<\_x
sudo cat /etc/group | grep "^docker"
sudo cat /etc/gshadow | grep "^docker"
groups
_x

:<<\_c
newgrp docker  # only works in interactive mode

# this could be done, but it makes the script too complex
newgrp docker <<!
groups
!

to insure our user belongs to the 'docker' group before the provisioner runs, we provision docker two stages where the first stage installs docker and adds group 'docker' to $USER
then through the separate login of subsequent provisioner script, $USER membership in group 'docker' will be in effect
_c

# activate the changes to groups - we don't need this if we proceed to login as that user
newgrp docker  # use the group 'docker' for newly created files and directories
. guest-provisioner.sh  # newgrp creates a new shell - necessary since newgrp creates a new shell
_s

#sudo groupadd --force docker
#sudo usermod -aG docker $USER
_debug "$(groups)"

_debug hgwowoufs

true

}  # _docker_install()


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
if you need to push the image to it, you need to set the environment STANDALONE=true
if you need to use it as mirror, you have to set the environment MIRROR_SOURCEE=https://registry-1.docker.io and MIRROR_SOURCE_INDEX=https://index.docker.io when set STANDALONE=true
note: current only the registry v0.9.1 supports mirror. registry v2.0 can't support mirror but private registry now. maybe registry 2.1/docker 1.7 will support mirror
https://github.com/duffqiu/docker-registry-systemd
_c

function _registry_server_insecure() {  # <- $DOCKER_REGISTRY_DATA_PATH

#echo "_registry_server_insecure"

_debug "DOCKER_REGISTRY_DATA_PATH: $DOCKER_REGISTRY_DATA_PATH"
_debug2 "$(ls -l $DOCKER_REGISTRY_DATA_PATH)"

:<<\_c
https://docs.openstack.org/kolla-ansible/4.0.0/quickstart.html
_c
:<<\_s
mtu=$(cat /sys/class/net/$MNIC_IFACE/mtu)
if (( mtu < 1500 )); then mtu="--mtu $mtu"; else mtu="";
_s

sudo bash -c "cat > /etc/systemd/system/docker-registry.service" <<!

[Unit]
Description=docker private insecure registry
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill registry
ExecStartPre=-$(which docker) rm registry

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  -v "$DOCKER_REGISTRY_DATA_PATH":$INTERNAL_WORK_DIR \
  -p ${DOCKER_REGISTRY[port]}:$INTERNAL_REGISTRY_PORT \
  -e REGISTRY_STORAGE_DELETE_ENABLED=true \
  $REGISTRY_IMAGE

ExecStop=/usr/bin/docker stop registry
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!

sudo systemctl start docker-registry.service

# return $?
}


:<<\_c
if you need to push the image to it, you need to set the environment STANDALONE=true
https://github.com/duffqiu/docker-mirror-systemd
_c

:<<\_x
  -e "STANDALONE=true" \
  -e "MIRROR_SOURCE=https://registry-1.docker.io" \
  -e "MIRROR_SOURCE_INDEX=https://index.docker.io" \
  -e "SETTINGS_FLAVOR=local" \

  -e "http_proxy=http://10.175.250.81:8080" \
  -e "https_proxy=http://10.175.250.81:8080" \

  --dns 146.11.115.200 \
_x

:<<\_x
sudo systemctl status docker-registry.service
sudo docker ps
sudo lsof -i:${DOCKER_REGISTRY[port]}

docker info | grep "Registry:"
docker search kolla
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
add a docker registry that may be accessed remotely
In order to make your registry accessible to external hosts, you must first secure it using TLS.

https://docs.docker.com/registry/deploying/
_c

function _registry_server_secure() {

echo fixme
return 1

_debug "_registry_server_secure"

#_registry_client_secure || return 1  # configure so localhost can access the registry

#DOCKER_REGISTRY_DATA_PATH=${DOCKER_REGISTRY_DATA_PATH:-/mnt/d-registry-cache}
#sudo mkdir $DOCKER_REGISTRY_DATA_PATH &>/dev/null && sudo chown $USER:$USER $DOCKER_REGISTRY_DATA_PATH

_debug2 "$(ls -l $DOCKER_REGISTRY_DATA_PATH)"

PICASSO_CERTS_PATH=/etc/docker/picasso
sudo mkdir -p $PICASSO_CERTS_PATH


# ----------
method=letsencrypt

case $method in

letsencrypt)

sudo cp $LETSENCRYPT_CERTS_PATH/privkey.pem $PICASSO_CERTS_PATH/domain.key
sudo cp $LETSENCRYPT_CERTS_PATH/fullchain.pem $PICASSO_CERTS_PATH/domain.cert
#sudo bash -c "cat $LETSENCRYPT_CERTS_PATH/cert.pem $LETSENCRYPT_CERTS_PATH/chain.pem > $PICASSO_CERTS_PATH/domain.cert"
:<<\_x
openssl rsa -in $LETSENCRYPT_CERTS_PATH/privkey.pem -pubout > key.pub
_x
;;

self-signed)

sudo bash <<weak!

1>/dev/null pushd $PICASSO_CERTS_PATH

openssl genrsa 1024 > domain.key
chmod 400 domain.key

cat > $PICASSO_CERTS_PATH/domain.ini <<!
[ req ]
prompt = no
default_bits = 1024
distinguished_name = req_distinguished_name
req_extensions = req_ext

[ req_distinguished_name ]
C=CA
ST=Ontario
L=Toronto
O=Picasso Digital
OU=IT Department
CN=picasso.digital

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = $DOMAIN
DNS.2 = $HOSTNAME
IP.1 = $MNIC_IP
!

openssl req -config $PICASSO_CERTS_PATH/domain.ini -new -x509 -nodes -sha1 -days 365 -key domain.key -out domain.cert  # Generate certificate

1>/dev/null popd

weak!
;;

esac

_debug2 "$(ls -l $PICASSO_CERTS_PATH)"


# ----------
method=tls  # basic|tls

case $method in

basic)
echo UNTESTED sofswlwsooss

REGISTRY_USER=${REGISTRY_USER:-picasso}
REGISTRY_PASS=${REGISTRY_PASS:-$REGISTRY_USER}

_debug "REGISTRY_USER: $REGISTRY_USER, REGISTRY_PASS: $REGISTRY_PASS"

_install apache2-utils

:<<\_c
htpasswd [-cimBdpsDv] [-C cost] passwordfile username
-c  Create a new file.
-i  Read password from stdin without verification (for script usage).
_c
:<<\_c
https://stackoverflow.com/questions/67702845/docker-private-registry-connection-refused
_c

echo "$REGISTRY_PASS" | sudo htpasswd -i -c PICASSO_CERTS_PATH/registry.password $REGISTRY_USER #&>/dev/null

sudo docker run \
  --entrypoint htpasswd \
  $REGISTRY_IMAGE -Bbn $REGISTRY_USER $REGISTRY_PASS > auth/htpasswd


# ----------
:<<\_s
mtu=$(cat /sys/class/net/$MNIC_IFACE/mtu)
if (( mtu < 1500 )); then mtu="--mtu $mtu"; else mtu="";
_s

sudo bash -c "cat > /etc/systemd/system/docker-registry.service" <<!

[Unit]
Description=docker private secure registry
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill registry
ExecStartPre=-$(which docker) rm registry

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  -v "$DOCKER_REGISTRY_DATA_PATH":$INTERNAL_WORK_DIR \
  -v PICASSO_CERTS_PATH:/auth \
  -e "REGISTRY_AUTH=htpasswd" \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
  -p ${DOCKER_REGISTRY[port]}:$INTERNAL_REGISTRY_PORT \
  -e REGISTRY_STORAGE_DELETE_ENABLED=true \
  $REGISTRY_IMAGE

ExecStop=/usr/bin/docker stop registry
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!
;;

tls)

:<<\_s
mtu=$(cat /sys/class/net/$MNIC_IFACE/mtu)
if (( mtu < 1500 )); then mtu="--mtu $mtu"; else mtu="";
_s

_debug "$(sudo ls -l /etc/letsencrypt/live/$FQDN/)"  # symlinks to our active certs

sudo bash -c "cat > /etc/systemd/system/docker-registry.service" <<!

[Unit]
Description=docker private secure registry
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill registry
ExecStartPre=-$(which docker) rm registry

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  -v "$DOCKER_REGISTRY_DATA_PATH":$INTERNAL_WORK_DIR \
  -v $PICASSO_CERTS_PATH:$INTERNAL_CERTS_PATH \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=$INTERNAL_CERT \
  -e REGISTRY_HTTP_TLS_KEY=$INTERNAL_KEY \
  -p ${DOCKER_REGISTRY[port]}:$INTERNAL_REGISTRY_PORT \
  -e REGISTRY_STORAGE_DELETE_ENABLED=true \
  $REGISTRY_IMAGE

ExecStop=/usr/bin/docker stop registry
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!
:<<\_x
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -p ${DOCKER_REGISTRY[port]}:443 \
_x
;;

esac

_debug "$(groups)"

sudo systemctl start docker-registry.service

# return $?
}


:<<\_x
sudo docker ps
sudo bash -c "echo $REGISTRY_PASS | docker login --password-stdin -u $REGISTRY_USER $MNIC_IP:${DOCKER_REGISTRY[port]}"
sudo bash -c "echo $REGISTRY_PASS | docker login --password-stdin -u $REGISTRY_USER www.picasso.digital:${DOCKER_REGISTRY[port]}"
_x

:<<\_x
sudo bash -c "echo $REGISTRY_PASS | docker login --password-stdin -u $REGISTRY_USER www.picasso.digital:${DOCKER_REGISTRY[port]}"
sudo docker pull hello-world
sudo docker tag hello-world $DOCKER_REGISTRY/my-hello-world
sudo docker push $DOCKER_REGISTRY/my-hello-world
sudo docker pull $DOCKER_REGISTRY/my-hello-world
_x

:<<\_x
# test...
sudo docker pull hello-world
sudo docker tag hello-world $DOCKER_REGISTRY/my-hello-world
sudo docker push $DOCKER_REGISTRY/my-hello-world
sudo docker pull $DOCKER_REGISTRY/my-hello-world
_x

:<<\_x
chrome https://www.picasso.digital  # test TLS

DOCKER_REGISTRY[port]=5000
sudo docker pull hello-world

DOCKER_REGISTRY=www.picasso.digital:${DOCKER_REGISTRY[port]}
REGISTRY_USER=picasso
REGISTRY_PASS=picasso

sudo docker image list
_x

:<<\_x
sudo systemctl status docker-registry.service
sudo docker ps
sudo lsof -i:${DOCKER_REGISTRY[port]}
_x

:<<\_x
docker info | grep "Registry:"
docker search kolla
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _registry_server() {  # <- $DOCKER_REGISTRY_DATA_PATH

local scheme=$1

case $scheme in

http)
_registry_client $scheme || return 1  # configure so localhost can access the registry
_registry_server_insecure
;;

https)
_registry_server_secure
;;

esac
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
restarting docker purges all existing docker processes
with systemd:Restart=always, our registry service will automatically restart
without systemd:Restart=always, we must restart docker before starting our registry
_c

function _registry_client() {

_debug "_registry_client"

local scheme=$1

case $scheme in

http)
[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime

sudo bash -c "echo '$(jq ". + {\"insecure-registries\": [\"$DOCKER_REGISTRY\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"

sudo systemctl reload docker.service
:<<\_x
jq 'has("insecure-registries")' /etc/docker/daemon.json
_x
;;

https)
:<<\_s
# there is no 'secure-registries' key - private registries are assumed to be secure unless configured with 'insecure-registries'

[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime

sudo bash -c "echo '$(jq ". + {\"secure-registries\": [\"https://$DOCKER_REGISTRY\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"

sudo systemctl reload docker.service
_s
:<<\_x
jq 'has("secure-registries")' /etc/docker/daemon.json
_x
;;

esac

# return $?
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
restarting docker purges all existing docker processes
with systemd:Restart=always, our registry service will automatically restart
without systemd:Restart=always, we must restart docker before starting our registry
_c

function _podman_registry_client() {

_debug "_registry_client"

local scheme=$1

case $scheme in

http)
[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime

sudo bash -c "echo '$(jq ". + {\"insecure-registries\": [\"$DOCKER_REGISTRY\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"

sudo systemctl reload docker.service
:<<\_x
jq 'has("insecure-registries")' /etc/docker/daemon.json
_x
;;

https)
:<<\_s
# there is no 'secure-registries' key - private registries are assumed to be secure unless configured with 'insecure-registries'

[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime

sudo bash -c "echo '$(jq ". + {\"secure-registries\": [\"https://$DOCKER_REGISTRY\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"

sudo systemctl reload docker.service
_s
:<<\_x
jq 'has("secure-registries")' /etc/docker/daemon.json
_x
;;

esac

# return $?
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _mirror_server_insecure() {

_debug "_mirror_server_insecure"

_url_parse $DOCKERHUB_ENDPOINT DOCKERHUB


# ----------
sudo mkdir -p /etc/docker/mirror

EXTERNAL_CONFIG_YML=/etc/docker/mirror/config.yml

:<<\_c
https://docs.docker.com/registry/configuration/
https://github.com/distribution/distribution/blob/main/cmd/registry/config-example.yml
_c
sudo bash -c "cat > $EXTERNAL_CONFIG_YML" <<!
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: $INTERNAL_WORK_DIR
http:
  addr: :$INTERNAL_REGISTRY_PORT

  host: $DOCKERHUB_ENDPOINT
  relativeurls: false
  draintimeout: 60s
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
proxy:
  remoteurl: https://registry-1.docker.io
!


# ----------
sudo bash -c "cat > /etc/systemd/system/docker-mirror.service" <<!

[Unit]
Description=docker mirror
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill docker-mirror
ExecStartPre=-$(which docker) rm docker-mirror

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  --mount type=bind,src=$EXTERNAL_WORK_DIR,dst=$INTERNAL_WORK_DIR \
  -p ${DOCKERHUB[port]}:$INTERNAL_REGISTRY_PORT \
-v $EXTERNAL_CONFIG_YML:$INTERNAL_CONFIG_YML \
$REGISTRY_IMAGE

ExecStop=/usr/bin/docker stop docker-mirror
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!
#  -e REGISTRY_PROXY_REMOTEURL=https://mirror.gcr.io
#  -e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
#  -v $EXTERNAL_WORK_DIR:$INTERNAL_WORK_DIR \

sudo systemctl start docker-mirror.service
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _mirror_server_secure() {


_debug "_mirror_server LETSENCRYPT_CERTS_PATH: $LETSENCRYPT_CERTS_PATH, DOCKERHUB: $DOCKERHUB"

_debug3 "$(sudo ls -l $LETSENCRYPT_CERTS_PATH)"

_url_parse $DOCKERHUB_ENDPOINT DOCKERHUB


# ----------
#EXTERNAL_CERTS_PATH=/etc/docker/certs.d/$DOCKERHUB
EXTERNAL_CERTS_PATH=/etc/docker/certs.d/${DOCKERHUB[hostname]}
sudo mkdir -p $EXTERNAL_CERTS_PATH

:<<\_c
DOCKERHUB=localhost:$INTERNAL_REGISTRY_PORT
DOCKERHUB=www.picasso.digital:5001

/etc/docker/
           +-- daemon.json
           +-- key.json
           +-- certs.d/        <-- Certificate directory
                      +-- $DOCKERHUB/          <-- Hostname:port - $EXTERNAL_CERTS_PATH
                                        +-- client.cert          <-- Client certificate
                                        +-- client.key           <-- Client key
                                        +-- ca.crt               <-- Certificate authority that signed the registry certificate

/etc/ssl/certs/ca-certificates.crt is the local CA certificate store (iow: certs from trusted Certificate Authorities)
whatever certificates it contains, the local system will trust
server certs are signed by one of these CA certs and then these CA certs are used to verify server certificates are valid
_c

if sudo test -d "$LETSENCRYPT_CERTS_PATH"; then

_debug "letsencrypt"

sudo cp $LETSENCRYPT_CERTS_PATH/privkey.pem $EXTERNAL_CERTS_PATH/domain.key

:<<\_c
concatenate your certificate with the intermediate certificate to form a certificate bundle
https://docs.docker.com/registry/deploying/#get-a-certificate
cert.pem + chain.pem = fullchain.pem
_c
#sudo bash -c "cat $LETSENCRYPT_CERTS_PATH/cert.pem $LETSENCRYPT_CERTS_PATH/chain.pem > $EXTERNAL_CERTS_PATH/domain.cert"
sudo cp $LETSENCRYPT_CERTS_PATH/fullchain.pem $EXTERNAL_CERTS_PATH/domain.cert
sudo chown :docker -R $EXTERNAL_CERTS_PATH

else

_debug "self-signed"

CA_CERTS=/etc/ssl/certs/ca-certificates.crt
:<<\_x
curl --cacert $CA_CERTS ...
_x
:<<\_x
# extract the CA cert for a particular server
SERVER=google.com
SERVER=letsencrypt.org
SERVER=www.picasso.digital
openssl s_client -showcerts -servername $SERVER -connect $SERVER:443 #> cacert.pem
_x

:<<\_c
# self-signed cert
# self-signed certs require adding the certificate to these servers to pull images from their registries.
https://docs.docker.com/registry/insecure/
_c

:<<\_c
Self-signed certificates are not validated with any third party unless you import them to the browsers previously.
https://stackoverflow.com/questions/10175812/how-to-generate-a-self-signed-ssl-certificate-using-openssl/27931596#27931596
_c

:<<\_c
https://stackoverflow.com/questions/50768317/docker-pull-certificate-signed-by-unknown-authority

Linux: /etc/docker/certs.d/[domain of relevent cert]/[cert].crt
Windows/WSL1/WSL2: C:/ProgramData/Docker/certs.d/[domain of relevent cert]/[cert].crt
_c

:<<\_c
-nodes - (short for no DES) if you don't want to protect your private key with a passphrase

valid for $FQDN (SAN),
also valid for the IP address $MNIC_IP (SAN),

https://stackoverflow.com/questions/10175812/how-to-generate-a-self-signed-ssl-certificate-using-openssl/41366949#41366949
_c

sudo openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout $EXTERNAL_CERTS_PATH/domain.key -out $EXTERNAL_CERTS_PATH/domain.cert \
  -subj "/C=CA/ST=Ontario/L=Toronto/O=Picasso Digital/OU=Org/CN=$FQDN" \
  -addext "subjectAltName=DNS:$FQDN,IP:206.248.172.65"
#  -addext "subjectAltName=DNS:$FQDN,IP:$MNIC_IP"

:<<\_c
Copy the domain.cert file to /etc/docker/certs.d/myregistrydomain.com:$INTERNAL_REGISTRY_PORT/ca.crt on every Docker host.
https://docs.docker.com/registry/insecure/#/docker-still-complains-about-the-certificate-when-using-authentication
_c

:<<\_c
a self-signed cert is not automatically trusted by the OS - we have to tell the OS to trust the CA-cert used to sign our self-signed cert
When using authentication, some versions of Docker also require you to trust the certificate at the OS level.
https://docs.docker.com/registry/insecure/#/docker-still-complains-about-the-certificate-when-using-authentication
_c

:<<\_c
Make sure on the daemon that TLS client certificates have the ending ".cert", and CA certificates have the ending ".crt",
if you have ".key" without a ".cert" you will get that error, it will not attempt to use a ".crt" file.
_c

#sudo ln -f -s $CA_CERTS $EXTERNAL_CERTS_PATH/ca-certificates.crt
#sudo ln -f -s $CA_CERTS $EXTERNAL_CERTS_PATH/ca.crt

sudo chown :docker -R $EXTERNAL_CERTS_PATH

#sudo cp $PICASSO_CERTS_PATH/domain.cert /usr/local/share/ca-certificates/ca.crt
#sudo cp $PICASSO_CERTS_PATH/domain.cert /usr/local/share/ca-certificates/${FQDN}.dunno


#sudo bash -c "openssl s_client -showcerts -connect $DOCKERHUB < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /etc/docker/certs.d/$FQDN/ca.crt"
#sudo cp $EXTERNAL_CERTS_PATH/ca.crt /usr/local/share/ca-certificates/

sudo update-ca-certificates  # /usr/local/share/ca-certificates/${FQDN}.dunno -> $CA_CERTS

sudo systemctl restart docker.service
#sudo systemctl reload docker.service
fi

:<<\_x
FQDN=www.picasso.digital
DOCKERHUB[port]=5001
DOCKERHUB=$FQDN:${DOCKERHUB[port]}

CA_CERTS=/etc/ssl/certs/ca-certificates.crt
LETSENCRYPT_CERTS_PATH=/etc/letsencrypt/live/$FQDN
EXTERNAL_CERTS_PATH=/etc/docker/certs.d/$DOCKERHUB

ll $LETSENCRYPT_CERTS_PATH
ll $EXTERNAL_CERTS_PATH

systemctl cat docker-mirror.service

cat /etc/hosts

cat /etc/docker/daemon.json

docker pull ubuntu:14.04
sudo journalctl -u docker
```
Feb 24 19:14:04 www dockerd[13955]: time="2022-02-24T19:14:04.057688025-05:00" level=warning msg="Error getting v2 registry: Get https://www.picasso.digital:5001/v2/: dial tcp 192.168.1.10:5001: connect: connection refused"
```

sudo nano /etc/systemd/system/docker-mirror.service

docker pull $DOCKERHUB/ubuntu:14.04

EXTERNAL_WORK_DIR=$HOME/registry

ll $EXTERNAL_WORK_DIR/docker/registry/v2/repositories/library
_x

:<<\_x
sudo journalctl --rotate
sudo journalctl --vacuum-time=1s
docker -vvv pull www.picasso.digital:5001/ubuntu:14.04
sudo journalctl -xe --no-pager -u docker
_x

#READONLY_MNT=/mnt/readonly
#EXTERNAL_WORK_DIR=$READONLY_MNT/dockerhub-cache
#ll /usr/local/share/ca-certificates
#nc -nz -w 1 $MNIC_IP 5001

_debug

sudo chown :docker -R $EXTERNAL_CERTS_PATH


# ----------
method=tls  # basic|tls

case $method in

tls)

_debug "$(sudo ls -l /etc/letsencrypt/live/$FQDN/)"  # symlinks to our active certs

:<<\_s
mtu=$(cat /sys/class/net/$MNIC_IFACE/mtu)
if (( mtu < 1500 )); then mtu="--mtu $mtu"; else mtu="";
_s

:<<\_x
EXTERNAL_WORK_DIR=$HOME/registry
DOCKERHUB[port]=5001
FQDN=www.picasso.digital
DOCKERHUB=$FQDN:${DOCKERHUB[port]}
EXTERNAL_CERTS_PATH=/etc/docker/certs.d/$DOCKERHUB
_x

:<<\_c
To configure a Registry to run as a pull through cache, the addition of a proxy section is required to the config file.
https://docs.docker.com/registry/recipes/mirror/
_c
sudo mkdir -p /etc/docker/mirror

EXTERNAL_CONFIG_YML=/etc/docker/mirror/config.yml

:<<\_x
docker run -it --rm --entrypoint cat registry:2 $INTERNAL_CONFIG_YML > $EXTERNAL_CONFIG_YML
_x

:<<\_s
EXTERNAL_WORK_DIR=/mnt/readwrite/dockerhub
DOCKERHUB[port]=5001
REGISTRY_NAME=docker-mirror
REGISTRY_IMAGE=registry:2

sudo bash -c "cat > $EXTERNAL_CONFIG_YML" <<!
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: $INTERNAL_WORK_DIR
http:
  addr: 0.0.0.0:$INTERNAL_REGISTRY_PORT
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
proxy:
  remoteurl: https://registry-1.docker.io
!
:<<\_x
  addr: 0.0.0.0:$INTERNAL_REGISTRY_PORT
  tls:
    key: $INTERNAL_KEY
    certificate: $INTERNAL_CERT
_x

sudo bash -c "cat > /etc/systemd/system/docker-mirror.service" <<!

[Unit]
Description=docker mirror
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill docker-mirror
ExecStartPre=-$(which docker) rm docker-mirror

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  -v $EXTERNAL_WORK_DIR:$INTERNAL_WORK_DIR \
  -p 192.168.1.215:${DOCKERHUB[port]}:$INTERNAL_REGISTRY_PORT \
  -v $EXTERNAL_CONFIG_YML:$INTERNAL_CONFIG_YML \
$REGISTRY_IMAGE
#  --mount type=bind,src=$EXTERNAL_CERTS_PATH,dst=$INTERNAL_CERTS_PATH \

ExecStop=/usr/bin/docker stop docker-mirror
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!

sudo systemctl daemon-reload
sudo systemctl restart docker-mirror.service
sudo systemctl status docker-mirror.service
sudo lsof -i:5001
sudo iptables -L | grep 5001
nc -z -w1 $(_dns_lookup dockerhub) 5001
sudo netstat -tlp

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install nfs-common nmap
sudo nmap -A dockerhub


sudo mkdir -p /etc/containers

sudo bash -c "cat > /etc/containers/registries.conf" <<!
unqualified-search-registries = ['dockerhub:5001', 'docker.io']
!
_s

:<<\_c
https://docs.docker.com/registry/configuration/
https://github.com/distribution/distribution/blob/main/cmd/registry/config-example.yml
_c
sudo bash -c "cat > $EXTERNAL_CONFIG_YML" <<!
version: 0.1
log:
  accesslog:
    disabled: false
  level: debug
  formatter: text
  fields:
    service: registry
    environment: staging
storage:
  filesystem:
    rootdirectory: $INTERNAL_WORK_DIR
http:
  addr: 0.0.0.0:$INTERNAL_REGISTRY_PORT
  host: $DOCKERHUB_ENDPOINT
  relativeurls: true
  draintimeout: 60s
  tls:
    certificate: $INTERNAL_CERT
    key: $INTERNAL_KEY
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
proxy:
  remoteurl: https://registry-1.docker.io
!
:<<\_x
log:
  fields:
    service: registry
_x

:<<\_c
rather than this...
-v $EXTERNAL_CERTS_PATH:$INTERNAL_CERTS_PATH

we use this...
--mount type=bind,src=$EXTERNAL_CERTS_PATH,dst=$INTERNAL_CERTS_PATH

that is because $EXTERNAL_CERTS_PATH may contain a colon which confuses docker

iow this fails: -v /etc/docker/certs.d/www.picasso.digital:5001:$INTERNAL_CERTS_PATH
this works: --mount type=bind,src=/etc/docker/certs.d/www.picasso.digital:5001,dst=$INTERNAL_CERTS_PATH
_c
sudo bash -c "cat > /etc/systemd/system/docker-mirror.service" <<!

[Unit]
Description=docker mirror
After=docker.service

[Service]
ExecStartPre=-$(which docker) kill docker-mirror
ExecStartPre=-$(which docker) rm docker-mirror

ExecStart=/usr/bin/docker run --name $REGISTRY_NAME $mtu \
  --restart=always \
  --mount type=bind,src=$EXTERNAL_CERTS_PATH,dst=$INTERNAL_CERTS_PATH \
  --mount type=bind,src=$EXTERNAL_WORK_DIR,dst=$INTERNAL_WORK_DIR \
  -p ${DOCKERHUB[port]}:$INTERNAL_REGISTRY_PORT \
-v $EXTERNAL_CONFIG_YML:$INTERNAL_CONFIG_YML \
$REGISTRY_IMAGE

ExecStop=/usr/bin/docker stop docker-mirror
Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
!
:<<\_x
/usr/bin/docker run --name docker-mirror \
  --mount type=bind,src=/etc/docker/certs.d/dockerhub.runnable.org,dst=/certs \
  --mount type=bind,src=/mnt/readwrite/dockerhub-cache,dst=/var/lib/registry \
  -p 443:5000 \
  -v /etc/docker/mirror/config.yml:/etc/docker/registry/config.yml \
  registry:2
_x
:<<\_x
  --block-registry index.docker.io \

#  -e REGISTRY_STORAGE_DELETE_ENABLED=true \
#  -e REGISTRY_PROXY_REMOTE_USERNAME:$DOCKER_IO_USER \
#  -e REGISTRY_PROXY_REMOTE_PASSWORD:$DOCKER_IO_PASS \

  -e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=$INTERNAL_CERT \
  -e REGISTRY_HTTP_TLS_KEY=$INTERNAL_KEY \
  -v $EXTERNAL_WORK_DIR:$INTERNAL_WORK_DIR \
_x

;;

esac

sudo systemctl start docker-mirror.service
}


:<<\_x
sudo systemctl daemon-reload
sudo systemctl restart docker-mirror.service

sudo lsof -i:5001
systemctl cat docker-mirror.service

sudo systemctl status docker-mirror.service

docker info | grep "Registry:"
docker search kolla

sudo journalctl --rotate
sudo journalctl --vacuum-time=1s
sudo journalctl -u docker.service
_x

:<<\_x
REGISTRY_NAME=docker-mirror
REGISTRY_IMAGE=registry:2

sudo docker run -d \
  --name $REGISTRY_NAME \
  -v "$EXTERNAL_WORK_DIR":$INTERNAL_WORK_DIR \
  -p ${DOCKERHUB[port]}:$INTERNAL_REGISTRY_PORT \
  --registry-mirror $DOCKERHUB \
  $REGISTRY_IMAGE

EXTERNAL_CONFIG_YML=/etc/docker/mirror/config.yml
INTERNAL_CONFIG_YML=/etc/docker/registry/config.yml

cat $EXTERNAL_CONFIG_YML
docker run $REGISTRY_IMAGE cat $INTERNAL_CONFIG_YML  # show the image's default config.yml
docker run $REGISTRY_IMAGE ls -l $INTERNAL_WORK_DIR

sudo docker run \
  --name $REGISTRY_NAME \
  -v $EXTERNAL_CONFIG_YML:$INTERNAL_CONFIG_YML \
  $REGISTRY_IMAGE cat $INTERNAL_CONFIG_YML   # show the container's mounted config.yml

docker pull dockerhub.runnable.org:443/library/ubuntu

curl -s $DOCKERHUB_ENDPOINT/v2/library/ubuntu/tags/list | jq .
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
Pushing to a registry configured as a pull-through cache is unsupported.
_c


function _mirror_client() {

_debug "_mirror_client"

_url_parse $DOCKERHUB_ENDPOINT DOCKERHUB

sudo test -d /etc/docker || {

sudo mkdir /etc/docker
sudo chown :docker /etc/docker
}

[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime


case ${DOCKERHUB[scheme]} in

http)
sudo bash -c "echo '$(jq ". + {\"registry-mirrors\": [\"$DOCKERHUB_ENDPOINT\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"insecure-registries\": [\"${DOCKERHUB[host]}\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
;;

https)
sudo bash -c "echo '$(jq ". + {\"registry-mirrors\": [\"$DOCKERHUB_ENDPOINT\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
;;

esac

:<<\_x
sudo bash -c 'cat <<< $(jq "del(.\"registry-mirrors\")" /etc/docker/daemon.json) > /etc/docker/daemon.json'
sudo bash -c "echo '$(jq ". + {\"debug\": true}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
_x

:<<\_x
FQDN=www.picasso.digital
sudo bash -c "echo '$(jq ". + {\"tls\": true}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"tlscert\": \"$EXTERNAL_CERTS_PATH/domain.cert\"}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"tlskey\": \"$EXTERNAL_CERTS_PATH/domain.key\"}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
cat /etc/docker/daemon.json
_x

:<<\_s
sudo bash -c "cat > /etc/default/docker" <<!
DOCKER_OPTS="--config-file=/etc/docker/daemon.json"
!
_s

:<<\_s
# configure /etc/docker/daemon.json as above, or do this (both is not necessary)

sudo bash -c "cat >> /etc/default/docker" <<!
DOCKER_OPTS+=" --registry-mirror=http://$DOCKERHUB"
!
_s

sudo systemctl daemon-reload
sudo systemctl reload docker.service

sleep 1
:<<\_x
docker info | grep "Registry:"

docker pull ubuntu

curl -s $DOCKERHUB_ENDPOINT/v2/_catalog | jq .
_x
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
Pushing to a registry configured as a pull-through cache is unsupported.
_c


function _podman_mirror_client() {

_debug "_mirror_client"

_url_parse $DOCKERHUB_ENDPOINT DOCKERHUB

sudo test -d /etc/docker || {

sudo mkdir /etc/docker
sudo chown :docker /etc/docker
}

[[ -f "/etc/docker/daemon.json" ]] || sudo bash -c "echo '{}' > /etc/docker/daemon.json"  # prime


case ${DOCKERHUB[scheme]} in

http)
sudo bash -c "echo '$(jq ". + {\"registry-mirrors\": [\"$DOCKERHUB_ENDPOINT\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"insecure-registries\": [\"${DOCKERHUB[host]}\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
;;

https)
sudo bash -c "echo '$(jq ". + {\"registry-mirrors\": [\"$DOCKERHUB_ENDPOINT\"]}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
;;

esac

:<<\_x
sudo bash -c 'cat <<< $(jq "del(.\"registry-mirrors\")" /etc/docker/daemon.json) > /etc/docker/daemon.json'
sudo bash -c "echo '$(jq ". + {\"debug\": true}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
_x

:<<\_x
FQDN=www.picasso.digital
sudo bash -c "echo '$(jq ". + {\"tls\": true}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"tlscert\": \"$EXTERNAL_CERTS_PATH/domain.cert\"}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
sudo bash -c "echo '$(jq ". + {\"tlskey\": \"$EXTERNAL_CERTS_PATH/domain.key\"}" /etc/docker/daemon.json)' > /etc/docker/daemon.json"
cat /etc/docker/daemon.json
_x

:<<\_s
sudo bash -c "cat > /etc/default/docker" <<!
DOCKER_OPTS="--config-file=/etc/docker/daemon.json"
!
_s

:<<\_s
# configure /etc/docker/daemon.json as above, or do this (both is not necessary)

sudo bash -c "cat >> /etc/default/docker" <<!
DOCKER_OPTS+=" --registry-mirror=http://$DOCKERHUB"
!
_s

sudo systemctl daemon-reload
sudo systemctl reload docker.service

sleep 1
:<<\_x
docker info | grep "Registry:"

docker pull ubuntu

curl -s $DOCKERHUB_ENDPOINT/v2/_catalog | jq .
_x
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\__j
this is not valid if we implement a mirror

:<<\_c
If your docker registry is private and self hosted you should do the following...

docker login <REGISTRY_HOST>:<REGISTRY_PORT>
docker tag <IMAGE_ID> <REGISTRY_HOST>:<REGISTRY_PORT>/<APPNAME>:<APPVERSION>
docker push <REGISTRY_HOST>:<REGISTRY_PORT>/<APPNAME>:<APPVERSION>
  Using default tag: latest
  The push refers to repository [www.picasso.digital:5000/hello-world]
  Get "https://www.picasso.digital:5000/v2/": dial tcp 192.168.1.10:5000: connect: connection refused
_c

:<<\_c
To push an image to a private registry and not the central Docker registry you must tag it with the registry hostname and port (if needed).

https://docs.docker.com/engine/reference/commandline/tag/

docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
_c

:<<\_c
Push an image or a repository to a registry.

https://docs.docker.com/engine/reference/commandline/push/

docker push [OPTIONS] NAME[:TAG]
_c

function _docker_pull() {

_debug "_docker_pull DOCKER_REGISTRY: $DOCKER_REGISTRY"

case $DOCKER_REGISTRY in

localhost*)
docker pull $@
;;

*)

echo $@ | awk -F':' '{print $1,$2}' | while read -r image tag; do

image_tag=$image
[[ -n "$tag" ]] && image_tag+=":$tag"

_debug "image: $image, tag: $tag, image_tag: $image_tag"

docker pull $image_tag  # -> /var/lib/docker/overlay2

docker tag $image_tag $DOCKER_REGISTRY/$image_tag

docker push $DOCKER_REGISTRY/$image_tag  # -> registry:$DOCKER_REGISTRY_DATA_PATH

done
;;

esac
}
__j


:<<\_x
client...
. docker.fun && _docker_install

docker pull ubuntu

_docker_pull ubuntu

docker images
  REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
  192.168.1.200:5000/ubuntu   latest    ba6acccedd29   2 months ago   72.8MB
  ubuntu                      latest    ba6acccedd29   2 months ago   72.8MB

registry...
sudo ls -l /mnt/d-registry-cache/docker/registry/v2/repositories/ubuntu
sudo ls -l /mnt/d-registry-cache/docker/registry/v2/blobs/sha256/ba/ba6acccedd2923aee4c2acc6a23780b14ed4b8a5fa4e14e252a23b846df9b6c1
_x


:<<\_x
# client-side

DOCKER_CERT_PATH=~/.docker  # default: ~/.docker
mkdir -p $DOCKER_CERT_PATH

CA_CERTS=/etc/ssl/certs/ca-certificates.crt
sudo cp $CA_CERTS $DOCKER_CERT_PATH/ca.pem

sudo cp $LETSENCRYPT_CERTS_PATH/privkey.pem $DOCKER_CERT_PATH/domain.key
sudo cp $LETSENCRYPT_CERTS_PATH/fullchain.pem $DOCKER_CERT_PATH/domain.cert

cp 
_x



:<<\_j
:<<\_c
https://docs.openstack.org/kolla-ansible/4.0.0/quickstart.html
_c
mtu=$(cat /sys/class/net/$MNIC_IFACE/mtu)

(( mtu < 1500 )) && MTU="--mtu $mtu"

sudo mkdir -p /etc/systemd/system/docker.service.d

sudo bash -c "cat > /etc/systemd/system/docker.service.d/kolla.conf" <<!
[Service]
MountFlags=shared
ExecStart=
ExecStart=/usr/bin/docker daemon -H fd:// --insecure-registry=${DOCKERHUB[host]} $MTU
!

:<<\_x
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://127.0.0.1:2375
_x

sudo systemctl daemon-reload
sudo systemctl restart docker.service
_j


:<<\_x
# connection refused...

DOCKERHUB[hostname]=www.picasso.digital
DOCKERHUB_IP=206.248.172.65
DOCKERHUB[port]=5001
nc -v $DOCKERHUB_IP ${DOCKERHUB[port]}

# are we able to access the registry?

curl https://www.picasso.digital:5001/v2/_catalog


sudo systemctl stop docker-mirror.service
docker container rm --force docker-mirror
_x

:<<\_x
docker logs --tail 20 docker-mirror
_x

:<<\___j
which docker 1>/dev/null || {

which jq 1>/dev/null || _install jq

:<<\_c
method=pkg
this is the distro method that ubuntu ceph-absible is compatible with
_c
#method=apt-key  # deprecated
#method=pkg  # distro-method
#method=gpg
#method=script
method=test

case $method in

kolla)
curl -sSL https://get.docker.io | sudo bash
;;

test)

_install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
:<<\_x
ll /usr/share/keyrings/
_x


:<<\_c
Make sure /usr/share/keyrings/docker-archive-keyring.gpg is global readable.
the gpg file must be 'o+r', we could run 'chmod o+r'; however that would not catch the situation where the permission change does not take due to disk mount conditions or the like
here, we explicitly check for the right conditions and report if there was a problem
_c
sudo -u nobody test -r /usr/share/keyrings/docker-archive-keyring.gpg || _error 'sudo -u nobody test -r /usr/share/keyrings/docker-archive-keyring.gpg'

#echo \
#  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
#  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

:<<\_s
FAIL...
echo \
  "deb https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null
_s

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

_install docker-ce docker-ce-cli containerd.io

sudo rm -f /etc/apt/sources.list.d/docker.list
;;

pkg)
_install docker.io

sudo usermod -aG docker $USER
;;

gpg)

_install apt-transport-https ca-certificates curl gnupg lsb-release

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key --keyring /etc/apt/trusted.gpg.d/docker-apt-key.gpg add
:<<\_x
sudo gpg --keyserver subkeys.pgp.net --recv-keys 0EBFCD88

sudo apt-key fingerprint 0EBFCD88 1>/dev/null || _exit 'apt-key fingerprint 0EBFCD88'
_x

:<<\_c
Kolla-ansible adds the Docker repo to the system's sources list as follows...
echo "deb https://download.docker.com/linux/ubuntu focal stable" > /etc/apt/sources.list.d/docker.list

Docker documentation says to add the Docker repo to the system's sources list as follows...
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" > /etc/apt/sources.list.d/docker.list

the difference betwen these source list entries results in this error...
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}

to circumvent this issue, rather than create a source list entry like Docker documentation recommends, we create a source list entry like the Kolla-ansible one, since we have to conform to kolla-ansible's automation methods

using this...
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

retults in this:
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}

using this...
echo "deb https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

results in this:
    default: W: GPG error: https://download.docker.com/linux/ubuntu focal InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 7EA0A9C3F273FCD8
    default: E: The repository 'https://download.docker.com/linux/ubuntu focal InRelease' is not signed.

therefore, the only way i see we can follow Docker's documented install is to use their recommended settings and then remove the sources list after installation is complete
_c

#echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/trusted.gpg.d/docker-apt-key.gpg] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list 1>/dev/null

:<<\_x
cat /etc/apt/sources.list.d/docker.list
_x

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

:<<\_c
docker-ce is provided by docker.com, docker.io is provided by Debian
https://stackoverflow.com/questions/45023363/what-is-docker-io-in-relation-to-docker-ce-and-docker-ee-now-called-mirantis-k#:~:text=The%20accepted%20answer%20is%20under,repository%20from%20docker.com%20beforehands.
_c
_install docker-ce docker-ce-cli containerd.io

sudo rm /etc/apt/sources.list.d/docker.list  # see above comment

sudo usermod -aG docker $USER
;;

apt-key)

_install software-properties-common

#wget -qO - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo apt-add-repository -y 'deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable'

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
#deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable

#sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

:<<\_s
sudo apt --yes --no-install-recommends install apt-transport-https ca-certificates
wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release --codename --short) stable"
_s

#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-ce-archive-keyring.gpg 1>/dev/null

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update

_install docker.io

;;

script)

:<<\_c
this results in...
  Error: E:Conflicting values set for option Signed-By regarding source https://download.docker.com/linux/ubuntu/ focal: /usr/share/keyrings/docker-archive-keyring.gpg != , E:The list of sources could not be read.\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}
_c

wget -qO- https://get.docker.com/ | sudo sh || _exit "sudo sh"

;;

esac


# ----------
sudo systemctl --now enable containerd.service
sudo systemctl --now enable docker.service
:<<\_x
sudo systemctl status containerd.service
sudo systemctl status docker.service
_x

:<<\__j
#sudo mkdir -p /usr/lib/systemd/system/docker.service.d
#sudo bash -c "cat > /usr/lib/systemd/system/docker.service.d/override.conf" <<!
#ExecStart=/usr/bin/dockerd -H unix:///var/run/docker.sock -H tcp://0.0.0.0:2375 -H tcp://0.0.0.0:2376
#!
#sudo bash -c "cat > /etc/systemd/system/docker.service.d/override.conf" <<!

:<<\_c
https://github.com/docker/for-linux/issues/989
_c
echo dockerdlfdllldlld
#sudo sed -i "s|^After=.*|& docker.socket|g" /usr/lib/systemd/system/docker.service

#sudo sed -i "s|fd://|unix://|g" /lib/systemd/system/docker.service
#sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
#sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
__j

:<<\_s
# this is default...

sudo mkdir -p /var/lib/docker

sudo bash -c "cat > /etc/docker/daemon.json" <<!
{ "data-root": "/var/lib/docker", "storage-driver": "overlay2" }
!

sudo systemctl daemon-reload
sudo systemctl restart docker.service
_s


:<<\_x
sudo dockerd --debug
_x

:<<\_c
docker works out of the box for 'root', but fails for other users due to the fact that they are not actvie members of the group 'docker'

does this overcome the need for user to be in the 'docker' group?

https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue
_c
#[[ "$PROVISIONER_USER" == 'root' ]] || sudo chmod 660 /var/run/docker.sock


:<<\_s
# ----------
# allow current user to use docker

_debug "USER: $USER"

sudo groupadd --force docker
sudo usermod -aG docker $USER
:<<\_x
sudo cat /etc/group | grep "^docker"
sudo cat /etc/gshadow | grep "^docker"
groups
_x

:<<\_c
newgrp docker  # only works in interactive mode

# this could be done, but it makes the script too complex
newgrp docker <<!
groups
!

to insure our user belongs to the 'docker' group before the provisioner runs, we provision docker two stages where the first stage installs docker and adds group 'docker' to $USER
then through the separate login of subsequent provisioner script, $USER membership in group 'docker' will be in effect
_c

# activate the changes to groups - we don't need this if we proceed to login as that user
newgrp docker  # use the group 'docker' for newly created files and directories
. guest-provisioner.sh  # newgrp creates a new shell - necessary since newgrp creates a new shell
_s

#sudo groupadd --force docker
#sudo usermod -aG docker $USER
_debug "$(groups)"
}
___j
